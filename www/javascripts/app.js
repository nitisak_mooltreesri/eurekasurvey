var app = angular.module('eureka', ['ionic']);

app.factory('Task', function () {
    // Hacer un ejeemplo con t odo para depues re-factorizar.
    //console.debug(persistence.store.websql);
    persistence.store.websql.config(persistence, 'MyTask', 'A database description', 5 * 1024 * 1024);

    var Task = persistence.define('Task', {
        result: "TEXT",
        upload: "BOOL"
    });

    Task.prototype.getResume = function (){
        return this.id + " " + this.name + " " + this.description + " " + this.done;
    };

    persistence.schemaSync();
    return Task;
});

app.controller('MainCtrl', function ($scope, $window, $ionicModal, $http, Task) {
	$http.defaults.headers.post["Content-Type"] = "application/json";
	$scope.survey = {
		"name" : "euraka",
		"max" : 40,
		"current" : 01,
        "complete" : 0
	};
    
    $scope.firstTime = 'yes';
	$scope.animate = "fade-in";
	$scope.question = new Date();
	$scope.position = {
		"lat":0,
		"long":0
	};

    $scope.click = function (){
        var tarea = new Task();
        //tarea.id = 1;
        tarea.result = [1,2,3,4,'#5-6-7#'];
        tarea.upload = true;

        console.debug(tarea);
        persistence.add(tarea);

        persistence.flush(function() {
            console.debug('Done flushing');
        });
    }

	// $scope.questionare.saved = new Date();

	$ionicModal.fromTemplateUrl('templates/modal.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.modal = modal;
	});

	$scope.openModal = function() {
		var tmp = new Date();
		navigator.geolocation.getCurrentPosition(function(position){ 
			$scope.position.lat = position.coords.latitude; 
			$scope.position.long = position.coords.longitude
		});
		$scope.modal.question = tmp.getDate();
    	$scope.modal.show();
  	};

    $scope.gender = "init value";
    $scope.party = "init value";
    $scope.person = "init value";
    $scope.serviceType = "init value";
    $scope.orderReason = "init value";
    $scope.frequency = "init value";
    $scope.frequenceTime = "init value";
    $scope.update = "init value";

	$scope.createResult = function() {
		var tarea = new Task();
        //tarea.id = 1;
        tarea.result = [
            $scope.gender,
            $scope.party,
            $scope.person,
            $scope.serviceType,
            $scope.orderReason,
            $scope.frequency,
            $scope.frequenceTime,
            $scope.update,
            $scope.position.lat,
            $scope.position.long
        ];
        
        tarea.upload = false;

        console.debug(tarea);
        persistence.add(tarea);

        persistence.flush(function() {
            console.debug('Done flushing');
        });
		
		var postData = {
            "1" : $scope.gender,
            "2" : $scope.party,
            "3" : $scope.person,
            "4" : $scope.serviceType,
            "5" : $scope.orderReason,
            "6" : $scope.frequency,
            "7" : $scope.frequenceTime,
            "8" : $scope.update,
            "9" : $scope.position.lat,
            "10": $scope.position.long
        };

		var postConfig = {
			headers:  {
				'Content-Type': 'application/text; charset=UTF-8'
			}
			};

        $http.post('http://192.168.1.37:3000/submit', postData).
            success(function(data, status, headers, config) {
                console.log("Success send data to server");
            }).
            error(function(data, status, headers, config) {
                console.log("Error connot sent data to server");
            });	

        $scope.survey.current++;
		$scope.modal.hide();
	};

	$scope.cancelResult = function() {
		$scope.modal.hide();
	};
    
    $scope.isFirstTime = function() {
        var res = true;
        if ($scope.firstTime === 'no') {
            res = false;
        } else {
            res = true;
        }
        return res;
    };
               
    $scope.isSingle = function() {
    	
    	if ($scope.party === 'single') {
    		$scope.person = "";
    		return true;
    	} else {
    		return false;
    	}
    };

    $scope.status = "none";

    $scope.sendResult = function() {
    	console.log("Invoke sendResult");
    	$http({method: 'JSONP', url: 'http://192.168.1.38:3000/upload'})
    		.success(function (data, status) {
    			console.log(data);
    			console.log(status);
    			$scope.status = status;
    		})
    		.error(function (data, status) {
    			console.log(data);
    			console.log(status);
    			$scope.status = status;
    		})
    };

	//
	// This is Example for returning modal to Main page.
	//
	// $scope.createContact = function(u) {        
 	//    $scope.contacts.push({ name: u.firstName + ' ' + u.lastName });
 	//    $scope.modal.hide();
 	//  };

});
