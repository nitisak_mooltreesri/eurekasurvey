    var mydb;
        
    function init(){
        document.addEventListener("deviceready", onDeviceReady, false);
    }     
            
    function onDeviceReady() {
        document.getElementById("device").innerHTML = "device is ready พร้อม!";
        mydb = window.openDatabase("db1", "1.0", "Database Test1", 100 * 1024);

        mydb.transaction(queryDB, onError);

    }

    function queryDB(tx) {
        tx.executeSql('SELECT * FROM mytable1 ', [], querySuccess, onError);

    }

    function querySuccess(tx, results) {

        var len = results.rows.length;
        var res = document.getElementById("result");

        
        var str = "";
        for (var i=0 ; i<len ; i++){
            str  += results.rows.item(i).id + " " + 
                results.rows.item(i).product + "<br/>"; // +

        }

        res.innerHTML = '<hr>' + str;
       //document.write('test');
    }    
    
    function onError(err) {
        alert("Error " + err.message + err.code);
    }
